#!/usr/bin/env bash

cmd="$1"
mac="$2"
ip="$3"
hostname="$4"
now=$(date +"%FT%T")
domain=$DNSMASQ_DOMAIN


if [[ "$cmd" = "add" ]]; then
  IFS='.' read -ra ip_addr <<< "$ip"
  host_name_from_ip="ip-${ip_addr[0]}-${ip_addr[1]}-${ip_addr[2]}-${ip_addr[3]}-internal"

  echo "$now | INFO | Adding host entry $ip $host_name_from_ip $hostname.$domain" >> /var/log/dnsmasq-dhcp-script.log
  echo "$ip $host_name_from_ip $hostname$domain" > /opt/dnsmasq/hostsdir/"$host_name_from_ip"
fi

if [[ "$cmd" = "del" ]]; then
  echo "$now | INFO | Releasing $ip but $host_name_from_ip $hostname entry will be kept" >> /var/log/dnsmasq-dhcp-script.log
fi
